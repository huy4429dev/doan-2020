<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\ToeicHistory;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Laravel\Socialite\Facades\Socialite;

class StudentController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'name'     => 'required|max:255',
                'email'    => 'required|unique:students|email',
                'password' => 'required',
                'phone'    => 'required',
            ]
        );


        if ($validator->fails()) {

            return response($validator->errors());
        }
        
        $student = new Student();
        $student           = new Student();
        $student->name     = $request->name;
        $student->email    = $request->email;
        $student->avatar   = '';
        $student->password = bcrypt($request->password);
        
        $student->save();

        unset($student->password);

        if($student){
            
            Session::put('user', $student);
            return response("true",200);
        }

        return response("Đăng ký tài khoản thất bại" , 400);

        
    }


    public function login(Request $request)
    {
        $user = Student::whereEmail($request->email)->first();
        if (!$user) {
            Session::put('studentErr', json_encode(["err" => "Sai tên đăng nhập hoặc mật khẩu "]));
            return redirect()->back();
        }

        if (Hash::check($request->password, $user->password)) {

            Session::put('user', $user);
        } else {

            Session::put('studentErr', true);
        }
        return redirect()->back();
    }


    public function logout(Request $request)
    {
        Session::forget('user');
        return redirect()->route('home');
    }

    public function notLogged()
    {
        return view('errors.login');
    }

    public function profile()
    {
        $histories = ToeicHistory::where('student_id', Session::get('user')->id)->get();
        return view('student-profile', ['histories' => $histories]);
    }


    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {
        
            
            $user = Socialite::driver('google')->user();

            $finduser = Student::where('google_id', $user->id)->first();

            if ($finduser) {

                Session::put('user', $user);
                return redirect()->route('home');

            } else {

                $student           = new Student();
                $student->name     = $user->name;
                $student->email    = $user->email;
                $student->avatar   = $user->avatar;
                $student->password = bcrypt('123456');

                $student->save();

                Session::put('user', $student);
                return redirect()->route('home');
            }
        
    }
}

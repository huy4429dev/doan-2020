<?php

namespace App\Http\Controllers\Listening;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Topic;
use Illuminate\Support\Facades\DB;

class PageListeningController extends Controller
{

    public function showlisten(){
        $listen =   DB::table('topics')->where('category_id', '=', 3)->get();

        $listen1 = DB::table('topics')->where('category_id', '=', 3)->where('level','=',1)->get();
        $listen2 = DB::table('topics')->where('category_id', '=', 3)->where('level','=',2)->get();
        $listen3 = DB::table('topics')->where('category_id', '=', 3)->where('level','=',3)->get();
        $listen4 = DB::table('topics')->where('category_id', '=', 3)->where('level','=',4)->get();
       
        return view(
            'listening.dashboard',
            [
                'listenLevel1' => $listen1,
                'listenLevel2' => $listen2,
                'listenLevel3' => $listen3,
                'listenLevel4' => $listen4,
                
                'all'           => $listen    
            ]
        );
    }
    public function topicDetail($id)
    {
        $topics    = Topic::where('category_id','3')->get();
        $topicName = Topic::where('category_id','3')->find($id)->title;
        $posts     = Topic::where('category_id','3')->find($id)->posts;   
        return view('listening.chi_tiet_chu_de', [
            'topics'    => $topics,
            'topicName' => $topicName,
            'posts'     => $posts
        ]);
    }
    public function getAnswer(Request $request, $id)
    {
        $answer = Post::find($id)->answer;
        if ($request->answer === $answer) {
            return response([
                'success' => 'Đáp án chính xác !',
                'level'   => Post::find($id)->level
            ]);
        } else {
            return response(['error' => 'Đáp án chưa chính xác, hãy thử nghe lại thật kỹ và đưa ra một đáp án khác !']);
        }
    }
}

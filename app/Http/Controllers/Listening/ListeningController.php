<?php

namespace App\Http\Controllers\Listening;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Topic;
use Illuminate\Support\Str;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;

class ListeningController extends Controller
{
    public function index(Request $request)
    {

        $topics = Topic::where('category_id', '=', 3)
            ->orderBy('id', 'DESC')
            ->paginate(6);
        return view(
            'admin.listening.danh_sach_chu_de',
            [
                'topics' => $topics

            ]
        );
    }
    public function getAllPost()
    {
        return view('admin.listening.danh_sach_bai_viet');
    }
    public function addTopic()
    {
        return view('admin.listening.them_moi_chu_de');
    }
    public function createTopic(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'title'     => 'required|unique:topics,title',
                'thumb' => 'required',
                'level' => 'required',


            ],
            [
                'level.required' => 'Bạn chưa nhập level',
                'title.required' => 'Bạn chưa nhập tiêu đề',
                'thumb.required' => 'Bạn chưa chọn hình ảnh',
                'title.unique'   => 'Tiêu đề đã tồn tại',
            ]
        );
       
        
        $topic = new Topic();
        $topic->fill($request->all());
        $topic->category_id = 3;
        if ($request->hasFile('thumb')) {
            $file = $request->file('thumb');
            $name = $file->getClientOriginalName();
            $Hinh = Str::random(4) . '----' . $name;
            $file->move('uploads/listening', $Hinh);
            $topic->thumb = $Hinh;
        } else {
            $topic->thumb = " ";
           
        }
        $topic->save();
        sleep(0.5);
        return redirect('admin/listening/')->with('thongbao', 'Thêm thành công');
    }
    public function showTopic(Request $request, $id)
    {
        return view(
            'admin.listening.cap_nhat_chu_de',
            [

                'topic' => Topic::where('id', '=', $id)->where('category_id', '=', '3')->first(),
            ]
        );
    }
    public function updateTopic(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'title'     => 'required',
                'level'     => 'required',
            ],
            [
                'title.required' => 'Bạn chưa nhập tiêu đề',
                'level.required' => 'Bạn chưa nhập tiêu đề',
            ]
        );
        
        $topic = Topic::find($id);
        $topic->category_id = 3;
        $topic->fill($request->all());
        if ($request->hasFile('thumb')) {
            $file = $request->file('thumb');
            $name = $file->getClientOriginalName();
            $Hinh = Str::random(4) . '----' . $name;
            $file->move('uploads/listening', $Hinh);
            $topic->thumb = $Hinh;
        } else {
            $topic->thumb = Topic::find($id)->thumb;
        }
        $topic->save();
        sleep(0.5);
        return redirect('admin/listening/')->with('thongbao', 'Cập nhật thành công');
    }
    public function deleteTopic($id)
    {


            $listening =  Topic::findOrFail($id);
            $listening->delete();
           
            return redirect('admin/listening/')->with('thongbao', 'Xóa thành công');;
    }
    public function searchTopic(Request $request)
    {

        return view(
            'admin.listening.tim_kiem_chu_de',
            [
                'topics' => Topic::where('title', 'like', '%' . $request->tl . '%')
                    ->where('category_id', 3)
                    ->paginate(6)

            ]
        );
    }
}

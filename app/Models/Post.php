<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $fillable = ['title','thumb','content','topic_id','word_type','pronounce','use','audio','level','audio_question','answer','desc'];
    public function topic(){
    	return $this->belongsTo('App\Models\Topic');
    }
}

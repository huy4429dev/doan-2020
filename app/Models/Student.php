<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
class Student extends Model
{
    use HasRoles;
    protected $fillable = ['name, email, avatar, password' ,'google_id'];
    protected $table    = 'students';
}

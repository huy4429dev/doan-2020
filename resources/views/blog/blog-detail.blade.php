@extends('layouts.app')
@section('title','Tin tức')
@section('content')
<main>
    <div class="main-content">
        <div class="container bgr-white">
            <div class="article-clean py-5">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-10 col-xl-8 offset-lg-1 offset-xl-2">
                            <div class="intro">
                                <h1 class="text-center">{{$blogDetail->title}}</h1>
                                <p class="text-center"><span class="by">by</span> <a href="#">{{$blogDetail->user->name}}</a><span class="date">Sept 8th, 2018 </span></p><img class="img-fluid" src="/uploads/article/{{$blogDetail->thumbnail}}">
                                <div class="fb-like my-3" data-href="http://localhost:8000/blog/{{$blogDetail->id}}" data-width="" data-layout="standard" data-action="like" data-size="large" data-share="true"></div>
                            </div>
                            <div class="text">
                                <p>{{$blogDetail->summary}}</p>
                                <p>
                                    {!!$blogDetail->content!!}
                                </p>
                            </div>
                            <div class="fb-comments" data-href="http://localhost:8000/blog/{{$blogDetail->id}}" data-numposts="5" data-width="100%"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</main>
<div id="fb-root"></div>
@endsection
@section('css')

<style>
    .article-clean {
        color: #56585b;
        background-color: #fff;
        font-family: 'Lora', serif;
        font-size: 14px;
    }

    .article-clean .intro {
        font-size: 16px;
        margin: 0 auto 30px;
    }

    .article-clean .intro h1 {
        font-size: 32px;
        margin-bottom: 15px;
        padding-top: 20px;
        line-height: 1.5;
        color: inherit;
        margin-top: 20px;
    }

    .article-clean .intro p {
        color: #929292;
        font-size: 12px;
    }

    .article-clean .intro p .by {
        font-style: italic;
    }

    .article-clean .intro p .date {
        text-transform: uppercase;
        padding: 4px 0 4px 10px;
        margin-left: 10px;
        border-left: 1px solid #ddd;
    }

    .article-clean .intro p a {
        color: #333;
        text-transform: uppercase;
        padding-left: 3px;
    }

    .article-clean .intro img {
        margin-top: 20px;
    }

    .article-clean .text p {
        margin-bottom: 20px;
        line-height: 1.45;
    }

    @media (min-width:768px) {
        .article-clean .text p {
            font-size: 16px;
        }
    }

    .article-clean .text h2 {
        margin-top: 28px;
        margin-bottom: 20px;
        line-height: 1.45;
        font-size: 16px;
        font-weight: bold;
        color: #333;
    }

    @media (min-width:768px) {
        .article-clean .text h2 {
            font-size: 20px;
        }
    }

    .article-clean .text figure {
        text-align: center;
        margin-top: 30px;
        margin-bottom: 20px;
    }

    .article-clean .text figure img {
        margin-bottom: 12px;
        max-width: 100%;
    }
</style>
@endsection
@section('script')
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=543121906576699&autoLogAppEvents=1" nonce="d5qMjXXL"></script>
@endsection
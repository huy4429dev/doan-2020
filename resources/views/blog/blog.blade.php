@extends('layouts.app')
@section('title','Tin tức')
@section('content')
<main class="my-5 pt-5 ">
    <div class="container">

        <!--Section: Jumbotron-->
        <section class="card blue-gradient wow fadeIn" style="background: linear-gradient(40deg, #45cafc, #0062cc) !important; border:none">

            <!-- Content -->
            <div class="card-body text-white text-center py-5 px-5 my-5">

                <h1 class="mb-4">
                    <strong style="font-size:34px">Học tiếng Anh miễn phí với English Pro</strong>
                </h1>
                <p>
                    <strong style="font-size:18px">IS BEST FREE EDUCATION </strong>
                </p>
                <p class="mb-4 py-4" style="font-size:18px">
                    Tổng hợp những bài viết chia sẻ kinh nghiệm học tiếng Anh của người đi trước <br /> giúp bạn rút ngắn được thời gian học tập.
                </p>

            </div>
            <!-- Content -->
        </section>
        <!--Section: Jumbotron-->

        <!--Section: Cards-->
        <section class="pt-5">


            <!--Grid row-->
            @foreach($blog as $item)
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-lg-5 col-xl-4 mb-4">
                    <!--Featured image-->
                    <div class="view overlay rounded z-depth-1-half">
                        <div class="view overlay">
                            <div>
                                <img class="img-fluid" src="uploads/article/{{$item->thumbnail}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-lg-7 col-xl-7 ml-xl-4 mb-4">
                    <h3 class="mb-3 font-weight-bold dark-grey-text">
                        <strong>{{$item->title}}</strong>
                    </h3>
                    <p class="grey-text">{{$item->summary}}.</p>
                    <p class="d-flex justify-content-between">
                        <strong>{{$item->user->name}}</strong> <strong>{{random_int(4,10)}} phút đọc.</strong>
                    </p>
                    <div class="d-flex justify-content-between">
                        <div>
                            <a href="/blog/{{$item->id}}" target="_blank" class="btn btn-primary btn-md">Read more
                                <i class="fas fa-play ml-2"></i>
                            </a>
                        </div>
                        <div>   
                            <div class="fb-like my-3" data-href="http://localhost:8000/blog/{{$item->id}}" data-width="" data-layout="button_count" data-action="like" data-size="large" data-share="false"></div>
                        </div>
                    </div>
                </div>
                <!--Grid column-->

            </div>
            <!--Grid row-->

            <hr class="mb-5">
            @endforeach
            <!--Pagination-->
            <nav class="d-flex justify-content-center wow fadeIn">
                {{$blog->links()}}
            </nav>
            <!--Pagination-->

        </section>
        <!--Section: Cards-->

    </div>
</main>
<div id="fb-root"></div>
@endsection

@section('style')
<style>
    h1,
    h3 {
        font-family: monospace;
    }
</style>
@endsection

@section('script')
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=543121906576699&autoLogAppEvents=1" nonce="d5qMjXXL"></script>
@endsection
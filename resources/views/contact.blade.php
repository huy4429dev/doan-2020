@extends('layouts.app')
@section('title','Liên hệ')
@section('content')
    <main>
        <div class="main-content">
            <div class="container bgr-white">
                <div class="article-nav-wrapper">
                    <nav class="article-nav-contact pb-5">
                        <ul class="text-center contact-feature">
                            <li data-id="contact-question" class="contact-question">
                                Đặt câu hỏi
                            </li >
                            @if(Session::has('user'))
                            <li data-id="my-contact" class="my-contact">
                                Câu hỏi của bạn
                            </li>
                            @endif
                            <li data-id="list-contact" class="list-contact">
                                Câu hỏi mới
                            </li>
                        </ul>
                    </nav>
                    <div class="full-toeic-test">
                        <div style="margin-bottom: 60px;">
                            <h1 class="test-name text-center">
                                Viết câu hỏi của bạn
                            </h1>
                            <div style="text-align: center;" class="pb-5">
                                <p>
                                    Bạn có thể hỏi bất kì điều gì về tiếng Anh <br /> (từ vựng, ngữ pháp, giải một câu hỏi cụ thể, kinh nghiệm học, ...)</p>
                            </div>
                            <div class="form-contact contact" id="contact-question">
                                @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                                @endif
                                <form action="{{route('student-contact')}}" method="POST">
                                    @csrf
                                    <div class="form-group row">
                                        <label for="colFormLabelSm" class="col-form-label col-form-label-sm" style="font-size: 20px;margin-left: 14px;    margin-right: 12px;">Tựa đề</label>
                                        <div>
                                            <input type="text" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Câu hỏi của bạn về vấn đề gì ?" style="    width: 628px;height: 36px;" name="title" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="content" id="" cols="30" rows="10" placeholder="Nội dung câu hỏi" name="content" required></textarea>
                                    </div>
                                    @if(Session::has('user'))
                                    <input type="text" value="{{Session::get('user')->id}}" hidden name="student_id" required>
                                    @endif
                                    <div class="form-group">
                                        <input class="btn btn-primary" type="submit" name="send-contact" value="Gửi câu hỏi">
                                    </div>
                                </form>
                            </div>
                            @if(Session::has('user'))
                            <div id="my-contact" class="my-contact contact" style="width:700px; margin:auto; display:none">
                                <table class="table table-striped ">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nội dung</th>
                                            <th scope="col">Câu trả lời</th>
                                            <th scope="col">Thời gian</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!$myContacts->isEmpty())
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach($myContacts as $contact)
                                        @if(empty($contact->answer))
                                        <tr style="background: #dee2e6;">
                                        @else
                                        <tr style="background: #fff;">
                                        @endif
                                            <th scope="row">{{$i}}</th>
                                            <td>{{$contact->content}}</td>
                                            <td width="300px">{{ empty($contact->answer) ? 'Chưa có câu trả lời ' : $contact->answer }}</td>
                                            <td>{{@$contact->created_at}}</td>
                                        </tr>
                                        @php
                                            $i++;
                                        @endphp
                                        @endforeach
                                        @else 
                                            <h4 class="text-warning text-center">Bạn chưa có câu hỏi nào !</h4>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            @endif
                            <div id="list-contact" class="all-contact contact" style="width:700px; margin:auto;padding-bottom: 50px;; display:none">
                                <table class="table table-striped ">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nội dung</th>
                                            <th scope="col">Câu trả lời</th>
                                            <th scope="col">Thời gian</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!$allContact->isEmpty())
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach($allContact as $contact)
                                        @if(empty($contact->answer))
                                        <tr style="background: #dee2e6;">
                                        @else
                                        <tr style="background: #fff;">
                                        @endif
                                            <th scope="row">{{$i}}</th>
                                            <td width="300px">{{$contact->content}}</td>
                                            <td>{{ empty($contact->answer) ? 'Chưa có câu trả lời ' : $contact->answer }}</td>
                                            <td>{{@$contact->created_at}}</td>
                                        </tr>
                                        @php
                                            $i++;
                                        @endphp
                                        @endforeach
                                        @else 
                                            <h4 class="text-warning text-center">Bạn chưa hỏi câu hỏi nào :D</h4>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
    </main>
@endsection
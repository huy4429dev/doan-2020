@extends('layouts.app')
@section('title','vocabulary detail')
@section('content')
    <main>
        <div class="main-content">
            <div class="container bgr-white">
                <div class="main-head">
                    <h2>Ngữ pháp PRO - Các chủ đề ngữ pháp</h2>
                </div>
                <div class="container">
                    <section id="not-completed" class="toeic-milestone ui-tabs-panel ui-widget-content ui-corner-bottom" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="false" style="">
                        <h2 class="learing-title">
                            <span class="bold">{{count($all)}} Chủ Đề</span>
                        </h2>
                        <div class="milestone-grammar-wrapper clearfix">
                            <h3>Cấp độ 1 <span>⭐</span></h3>
                            @foreach($gramarsLevel1 as $gr1)
                            <div class="block-milestone milestone-grammar">

                                <div class="inner">
                                    <div class="name">
                                        <img src="/uploads/gramar/{{$gr1->thumb}}">
                                        <h3>{{$gr1->title}}</h3>
                                    </div>
                                    <a href="/ngu-phap-tieng-anh/{{$gr1->id}}/{{$gr1->title}}.html" class="button-link style-1" title="Kích để học">học</a>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <div class="milestone-grammar-wrapper clearfix">
                            <h3>Cấp độ 2 <span>⭐⭐</span></h3>
                            @foreach($gramarsLevel2 as $gr2)
                            <div class="block-milestone milestone-grammar">
                                <div class="inner">
                                    <div class="name">
                                        <img src="/uploads/gramar/{{$gr2->thumb}}">
                                        <h3>{{$gr2->title}}</h3>
                                    </div>
                                    <a href="/ngu-phap-tieng-anh/{{$gr2->id}}/{{$gr2->title}}.html" class="button-link style-1" title="Kích để học">học</a>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="milestone-grammar-wrapper clearfix">
                            <h3>Cấp độ 3 <span>⭐⭐⭐</span></h3>
                            @foreach($gramarsLevel3 as $gr3)
                            <div class="block-milestone milestone-grammar">
                                <div class="inner">
                                    <div class="name">
                                        <img src="/uploads/gramar/{{$gr3->thumb}}">
                                        <h3>{{$gr3->title}}</h3>
                                    </div>
                                    <a href="/ngu-phap-tieng-anh/{{$gr3->id}}/{{$gr3->title}}.html" class="button-link style-1" title="Kích để học">học</a>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <div class="milestone-grammar-wrapper clearfix">
                            <h3>Cấp độ 4 <span>⭐⭐⭐⭐</span></h3>
                            @foreach($gramarsLevel4 as $gr4)
                            <div class="block-milestone milestone-grammar">
                                <div class="inner">
                                    <div class="name">
                                        <img src="/uploads/gramar/{{$gr4->thumb}}">
                                        <h3>{{$gr4->title}}</h3>
                                    </div>
                                    <a href="/ngu-phap-tieng-anh/{{$gr4->id}}/{{$gr4->title}}.html" class="button-link style-1" title="Kích để học">học</a>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="milestone-grammar-wrapper clearfix">
                            <h3>Cấp độ 5 <span>⭐⭐⭐⭐⭐</span></h3>
                            @foreach($gramarsLevel5 as $gr5)
                            <div class="block-milestone milestone-grammar">
                                <div class="inner">
                                    <div class="name">
                                        <img src="/uploads/gramar/{{$gr5->thumb}}">
                                        <h3>{{$gr5->title}}</h3>
                                    </div>
                                    <a href="/ngu-phap-tieng-anh/{{$gr5->id}}/{{$gr5->title}}.html" class="button-link style-1" title="Kích để học">học</a>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </main>
@endsection
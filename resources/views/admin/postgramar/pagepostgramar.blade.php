@extends('layouts.app')
@section('title','vocabulary detail')
@section('content')
    <main>
        <div class="main-content">
            <div class="container bgr-white">
                <ul class="route">
                    <li><a href="{{ route('showGramar') }}">Ngữ pháp tiếng Anh</a></li>
                    <li><a href="Ngữ pháp tiếng Anh">{{ $detailgramars->title}}</a></li>
                    <li>Bài này</li>
                </ul>
                <h1 class="title-post">
                    {{ $detailgramars->title }}
                </h1>
                <div class="d-flex justify-content-center">
                    <div>
                        <div class="fb-like my-3" data-href="http://localhost:8000/ngu-phap-tieng-anh/{{$detailgramars->id}}/{{$detailgramars->title}}" data-width="" data-layout="standard" data-action="like" data-size="large" data-share="true"></div>
                    </div>
                </div>
                <div class="img-thumbnail" style="text-align:center;border: none">
                    <img src="/uploads/gramar/{{$detailgramars->thumb}}">
                </div>
             
                <div class="post-content">
                    {!! $detailgramars->content !!}
                    <div class="fb-comments" data-href="http://localhost:8000/ngu-phap-tieng-anh/{{$detailgramars->id}}/{{$detailgramars->title}}" data-numposts="5" data-width="100%"></div>
                </div>
            </div>

        </div>

        </div>
    </main>
@endsection
@section('script')
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=543121906576699&autoLogAppEvents=1" nonce="d5qMjXXL"></script>
@endsection
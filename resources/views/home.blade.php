@extends('layouts.app')
@section('title','Trang chủ')
@section('content')
    <main>
        <div class="box-full" style="padding: 50px;">

            <div class="container">
                <div class="row">
                    <div class="slider" style="box-shadow: 1px 1px 7px 0px #00000045;">
                        <div id="demo" class="carousel slide" data-ride="carousel">
                            <ul class="carousel-indicators">
                                <li data-target="#demo" data-slide-to="0" class="active"></li>
                                <li data-target="#demo" data-slide-to="1"></li>
                                <li data-target="#demo" data-slide-to="2"></li>
                            </ul>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="uploads/image/learn-english-writing-grammar-online.png" alt="Los Angeles" style="width:1100px; height:618px">
                                    <div class="carousel-caption">
                                        <h3>English Pro</h3>
                                        <p>We will become "English Hero" </p>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img src="uploads/image/english-course.jpg" alt="Chicago" style="width:1100px; height:618px">
                                    <div class="carousel-caption" style="color: black;bottom: 26px;">
                                        <h3>English Pro</h3>
                                        <p>Step by step </p>
                                    </div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a class="carousel-control-next" href="#demo" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid full-bgr">
            <div class="content">
                <div class="learn">
                    <div class="container">
                        <h2 style="    padding: 30px 0 20px;">Chương Trình Học</h2>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="learn-cate box">
                                    <h2 class="slogan">
                                        Lấy lại nền tảng tiếng Anh căn bản
                                        <p>Bạn muốn <b>lấy lại nền tảng tiếng Anh</b> trước khi bắt đầu luyện thi?</p>
                                    </h2>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="one-cate">
                                                <div class="cate-ava">
                                                    <img src="./uploads/image/luyen_nghe_toeic.png" alt="ngu phap">
                                                </div>
                                                <div class="title">
                                                    <h3>
                                                        Cải thiện kĩ năng nghe tiếng Anh
                                                    </h3>
                                                    <p class="excerpt">
                                                        Vừa luyện nghe, vừa chơi game!
                                                    </p>
                                                </div>
                                                <div class="go-learn">
                                                    <a href="{{route('showlisten')}}">CẢI THIỆN KĨ NĂNG NGHE</a>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="one-cate">
                                                <div class="cate-ava">
                                                    <img src="./uploads/image/600_tu_vung_toeic.png" alt="ngu phap">
                                                </div>
                                                <div class="title">
                                                    <h3>
                                                        Học ngữ pháp tiếng Anh căn bản
                                                    </h3>
                                                    <p class="excerpt">
                                                        với Chương trình Ngữ Pháp PRO
                                                    </p>
                                                </div>
                                                <div class="go-learn">
                                                    <a href="{{ route('showGramar') }}">Học ngữ pháp căn bản</a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="one-cate" style="margin:auto">
                                            <div class="cate-ava">
                                                <img src="./uploads/image/ngu_phap_toeic.png" alt="ngu phap">
                                            </div>
                                            <div class="title">
                                                <h3>
                                                    Học 600 từ vựng hay gặp trong TOEIC
                                                </h3>
                                                <p class="excerpt">
                                                    được chia theo chủ đề, với đầy đủ thông tin về từ
                                                </p>
                                            </div>
                                            <div class="go-learn">
                                                <a href="tu-vung">HỌC 600 TỪ VỰNG TOEIC</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="toeic">
                                            <h3>Luyện thi TOEIC</h3>
                                            <p>Bạn <b>đã nắm được ngữ pháp và từ vựng</b> tiếng Anh căn bản?</p>
                                            <a href="toeic/danh-sach-de-thi" class="go-toeic">BẮT ĐẦU LUYỆN NGAY</a>
                                        </div>
                                    </div>

                                </div>
                                <div class="learn-today box">
                                    <h3>
                                        Đề xuất bạn học hôm nay
                                    </h3>
                                    <div class="card-learn">
                                        <div class="blog-learn">
                                            <div class="cate-ava">
                                                <img src="./uploads/gramar/{{$topicGramarRandom->thumb}}" alt="">
                                            </div>
                                            <h4>{{$topicGramarRandom->title}}</h4>
                                        </div>
                                        <div class="go-learn">
                                            <a href="ngu-phap-tieng-anh/{{$topicGramarRandom->id}}/{{$topicGramarRandom->title}}.html">HỌC</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="rank-user">
                                    @if(Session::has('user'))

                                    <div class="user-info box">
                                        <div class="user-head">
                                            <div class="user-avatar">
                                                <img src="uploads/image/{{ substr(Session::get('user')->avatar,0,4) == 'http' ? 'avatar-customer.jpg' : Session::get('user')->avatar }}" alt="avatar">
                                            </div>
                                            <h3 class="user-name">
                                                {{Session::get('user')->name}}
                                            </h3>
                                            <img src="./uploads/image/level_1.png" alt="" class="user-level">

                                        </div>
                                        <div class="user-deltail-socre">
                                            <!-- <div class="score-of-week">
                                                <p class="score-title">
                                                    Điểm tuần này
                                                </p>
                                                <p class="score">
                                                    0
                                                </p>
                                            </div>
                                            <div class="score-of-week">
                                                <p class="score-title">
                                                    Hạng tuần này
                                                </p>
                                                <p class="score">
                                                    > 100
                                                </p>
                                            </div> -->
                                            <div class="score-of-week">
                                                <p class="score-title">
                                                    Tổng điểm
                                                </p>
                                                <p class="score score-total" style="color: #87c52e;">
                                                    {{Session::get('user')->score}}
                                                </p>
                                            </div>
                                            <div class="score-of-week">
                                                <p class="score-title">
                                                    Xếp hạng
                                                </p>
                                                <p class="score-rank">
                                                    {{$userRank}}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="chart box" style="padding-bottom:12px">
                                        <div class="chart-head">
                                            <div class="chart-img">
                                                <img src="./uploads/image/icon_ranking.png" alt="">
                                            </div>
                                            <div class="chart-title">
                                                <h2>Bảng xếp hạng</h2>
                                                <img src="./uploads/image/icon_question.png" alt="">
                                            </div>
                                        </div>
                                        <div class="chart-list">
                                            <div class="chart-button">
                                                <button>Tổng Xếp hạng</button>
                                            </div>
                                            <ul class="chart-week">
                                                @php
                                                $i = 1;
                                                @endphp
                                                @foreach($users as $user)
                                                <li class="one-user">
                                                    <div class="rank-number">{{$i}}</div>
                                                    <div class="user-info-rank">
                                                        <img class="avartar" src="./uploads/image/{{$user->avatar}}" alt="">
                                                        <span class="user-name">
                                                            {{$user->name}}
                                                        </span>
                                                        <img class="medal" src="./uploads/image/level_70.png" alt="">
                                                    </div>
                                                    <div class="chart-score">
                                                        {{$user->score}}
                                                    </div>
                                                </li>
                                                @php
                                                $i++
                                                @endphp
                                                @endforeach
                                            </ul>
                                            <!-- <ul class="chart-total">
                                            <li class="one-user">
                                                <div class="rank-number">1</div>
                                                <div class="user-info-rank">
                                                    <img class="avartar" src="./uploads/image/hienWQ3.png" alt="">
                                                    <span class="user-name">
                                                            Hien
                                                     </span>
                                                    <img class="medal" src="./uploads/image/level_70.png" alt="">
                                                </div>
                                                <div class="chart-score">
                                                    8709
                                                </div>
                                            </li>
                                            <li class="one-user">
                                                <div class="rank-number">2</div>
                                                <div class="user-info-rank">
                                                    <img class="avartar" src="./uploads/image/hienWQ3.png" alt="">
                                                    <span class="user-name">
                                                            Hien
                                                     </span>
                                                    <img class="medal" src="./uploads/image/level_70.png" alt="">
                                                </div>
                                                <div class="chart-score">
                                                    8709
                                                </div>
                                            </li>
                                            <li class="one-user">
                                                <div class="rank-number">3</div>
                                                <div class="user-info-rank">
                                                    <img class="avartar" src="./uploads/image/hienWQ3.png" alt="">
                                                    <span class="user-name">
                                                            Hien
                                                     </span>
                                                    <img class="medal" src="./uploads/image/level_70.png" alt="">
                                                </div>
                                                <div class="chart-score">
                                                    8709
                                                </div>
                                            </li>
                                            <li class="one-user">
                                                <div class="rank-number">4</div>
                                                <div class="user-info-rank">
                                                    <img class="avartar" src="./uploads/image/hienWQ3.png" alt="">
                                                    <span class="user-name">
                                                            Hien
                                                     </span>
                                                    <img class="medal" src="./uploads/image/level_70.png" alt="">
                                                </div>
                                                <div class="chart-score">
                                                    8709
                                                </div>
                                            </li>
                                            <li class="one-user">
                                                <div class="rank-number">5</div>
                                                <div class="user-info-rank">
                                                    <img class="avartar" src="./uploads/image/hienWQ3.png" alt="">
                                                    <span class="user-name">
                                                            Hien
                                                     </span>
                                                    <img class="medal" src="./uploads/image/level_70.png" alt="">
                                                </div>
                                                <div class="chart-score">
                                                    8709
                                                </div>
                                            </li>
                                            <li class="one-user">
                                                <div class="rank-number">6</div>
                                                <div class="user-info-rank">
                                                    <img class="avartar" src="./uploads/image/hienWQ3.png" alt="">
                                                    <span class="user-name">
                                                            Hien
                                                     </span>
                                                    <img class="medal" src="./uploads/image/level_70.png" alt="">
                                                </div>
                                                <div class="chart-score">
                                                    8709
                                                </div>
                                            </li>
                                            <li class="one-user">
                                                <div class="rank-number">7</div>
                                                <div class="user-info-rank">
                                                    <img class="avartar" src="./uploads/image/hienWQ3.png" alt="">
                                                    <span class="user-name">
                                                            Hien
                                                     </span>
                                                    <img class="medal" src="./uploads/image/level_70.png" alt="">
                                                </div>
                                                <div class="chart-score">
                                                    8709
                                                </div>
                                            </li>
                                            <li class="one-user">
                                                <div class="rank-number">8</div>
                                                <div class="user-info-rank">
                                                    <img class="avartar" src="./uploads/image/hienWQ3.png" alt="">
                                                    <span class="user-name">
                                                            Hien
                                                     </span>
                                                    <img class="medal" src="./uploads/image/level_70.png" alt="">
                                                </div>
                                                <div class="chart-score">
                                                    8709
                                                </div>
                                            </li>
                                            <li class="one-user">
                                                <div class="rank-number">9</div>
                                                <div class="user-info-rank">
                                                    <img class="avartar" src="./uploads/image/hienWQ3.png" alt="">
                                                    <span class="user-name">
                                                            Hien
                                                     </span>
                                                    <img class="medal" src="./uploads/image/level_70.png" alt="">
                                                </div>
                                                <div class="chart-score">
                                                    8709
                                                </div>
                                            </li>
                                            <li class="one-user">
                                                <div class="rank-number">10</div>
                                                <div class="user-info-rank">
                                                    <img class="avartar" src="./uploads/image/hienWQ3.png" alt="">
                                                    <span class="user-name">
                                                            Hien
                                                     </span>
                                                    <img class="medal" src="./uploads/image/level_70.png" alt="">
                                                </div>
                                                <div class="chart-score">
                                                    8709
                                                </div>
                                            </li>
                                        </ul> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="blog">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 style="padding: 0 30px  20px 30px;">Tiếng Anh Mỗi Ngày Blog</h2>
                                <div class="blog-list box">
                                    
                                    <div class="row">
                                        @if(!$blogs->isEmpty())
                                        <div class="col-md-6">
                                            <div class="post-to-day">
                                                <div class="card card-box">
                                                    <a href="./blog/{{$blogs->first()->id}}"><img src="./uploads/article/{{$blogs->first()->thumbnail}}" class="card-img-top" alt="..."></a>
                                                    <div class="card-body">
                                                        <a href="./blog/{{$blogs->first()->id}}">
                                                            <h5 class="card-title box-title">{{$blogs->first()->title}}</h5>
                                                        </a>
                                                        <a href="./blog/{{$blogs->first()->id}}">
                                                            <p class="card-text box-text">{{$blogs->first()->summary}}</p>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="post-box">
                                                @foreach($blogs as $blog)
                                                @if($blog->id == $blogs->first()->id)
                                                @php
                                                continue;
                                                @endphp
                                                @endif
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="post">
                                                            <a href="./blog/{{$blog->id}}"><img src="./uploads/article/{{$blog->thumbnail}}" alt=""></a>
                                                            <h4 class="title-post" style="padding: 2px 22px;">
                                                                <a style="    color: gray;font-weight: 400;" href="./blog/{{$blog->id}}"> {{$blog->title}}</a>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EnglishPro - @yield('title')</title>
    <link rel="stylesheet" href="{{url('/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('/css/style.css')}}">
    <link rel="stylesheet" href="{{url('/font-anwesome/css/all.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('style')
</head>

<body>
    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="nav">
                        <div>
                            <div class="logo">
                                <h3><a href="{{route('home')}}">ENGLISH PRO</a></h3>
                            </div>
                            <ul class="page-member">
                                <li><a href="{{route('about')}}">Giới thiệu</a></li>
                                <li><a href="{{route('contact')}}">Đặt câu hỏi</a></li>
                                <li><a href="{{route('blog')}}">Tin tức</a></li>
                            </ul>

                        </div>
                        <div>
                            <div class="user-login">
                                @if(!Session::has('user'))
                                <a href="#" id="myBtn">ĐĂNG NHẬP </a>
                                @else

                                <img src="uploads/image/{{  empty(Session::get('user')->avatar) ? 'user_icon.png' : Session::get('user')->avatar }}" style="width: 42px;height: 42px;border-radius: 50%;position: relative;left: -5px;top: -3px;" ; alt="">
                                <span>{{Session::get('user')->name}}</span>
                                <ul class="user-info">
                                    <li><a href="{{route('student-profile')}}">Tài khoản</a></li>
                                    <li><a href="{{route('student-logout')}}">Đăng xuất</a></li>
                                </ul>
                                @endif
                            </div>
                            <div id="myModal" class="modal">

                                <!-- Modal content -->
                                <div class="modal-content">
                                    <span class="close">&times;</span>
                                    <!-- Default form login -->
                                    <!-- Default form login -->
                                    <form id="form-login" class="text-center border border-light p-5" method="POST" action = "{{route('student-login')}}" >
                                        @csrf
                                        <p class="h4 mb-4">Đăng nhập</p>
                                        @if(Session::has('studentErr'))
                                        <div class="alert alert-danger">Sai tên đăng nhập hoặc mật khẩu</div>
                                        @endif
                                        <!-- Email -->
                                        <input type="email" id="defaultLoginFormEmail" class="form-control mb-4" placeholder="E-mail" name="email">

                                        <!-- Password -->
                                        <input type="password" id="defaultLoginFormPassword" class="form-control mb-4" placeholder="Password" name="password">

                                        <div class="d-flex justify-content-around p-0">
                                            <div class="p-0">
                                                <!-- Forgot password -->
                                                <a href="">Quên mật khẩu ?</a>
                                            </div>
                                        </div>

                                        <!-- Sign in button -->
                                        <button class="btn btn-info btn-block my-4" type="submit">Đăng nhập</button>

                                        <!-- Register -->
                                        <p class="text-right">
                                            <a id="user-register" href="javascript:void(0)">Đăng ký tài khoản</a>
                                        </p>

                                        <!-- Social login -->
                                        <p>hoặc đăng nhập với:</p>

                                        <a href="{{url('/user/google')}}" class="mx-2" role="button"><i class="fab fa-google light-blue-text"></i></a>
                                        <a href="#" class="mx-2" role="button"><i class="fab fa-facebook-f light-blue-text"></i></a>


                                    </form>
                                    <!-- Default form login -->
                                    <!-- Default form register -->
                                    <form id="form-register" class="text-center border border-light p-5">
                                        <p class="h4 mb-4">Đăng ký</p>

                                        <div class="form-row mb-4" style="padding:0">
                                            <div class="col">
                                                <!-- Last name -->
                                                <input type="text" id="defaultRegisterFormLastName" class="form-control" placeholder="Username" name="name" value="admin123" >
                                            </div>
                                        </div>

                                        <!-- E-mail -->
                                        <input type="email" id="defaultRegisterFormEmail" class="form-control mb-4" placeholder="E-mail" name="email" value="admin123@gmail.com" >

                                        <!-- Password -->
                                        <input type="password" id="defaultRegisterFormPassword" class="form-control mb-4" placeholder="Password" aria-describedby="defaultRegisterFormPasswordHelpBlock" name="password" value="admin123">
                                     

                                        <!-- Phone number -->
                                        <input type="text" id="defaultRegisterPhonePassword" class="form-control" placeholder="Phone number" aria-describedby="defaultRegisterFormPhoneHelpBlock" name="phone" value="09823232">
                                        <button id="btnRegister" class="btn btn-info my-4 btn-block" type="button">Đăng ký</button>
                                        <p class="text-right">
                                            <a id="user-login" href="javascript:void(0)">Đăng nhập</a>
                                        </p>

                                    </form>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </header>
        @yield('content')
    <footer>
        <div id="bottom">
            <div class="container">
                <div data-footer-accordion="" class="footer-link">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="item">
                                <h3 class="link-title active">• Về Tiếng Anh Mỗi Ngày <span class="icon icon-bt_up hidden-md hidden-lg"></span></h3>
                                <div class="wrap-link show">
                                    <ul class="links">
                                        <li><a href="/about/?r=footer">Giới thiệu về Tiếng Anh Mỗi Ngày</a></li>
                                        <li>
                                            <a href="https:/tienganhmoingay.com/ket-qua-thi-toeic-hoc-vien/?r=footer" title="Kết quả thi TOEIC của học viên">Kết quả thi TOEIC của học viên</a>
                                        </li>
                                        <li><a href="/jobs/?r=footer" title="Tuyển dụng">Tuyển dụng</a></li>
                                        <li><a href="/terms/?r=footer">Điều khoản sử dụng</a></li>
                                        <li><a href="/privacy-policy/?r=footer">Chính sách bảo mật</a></li>
                                        <li><a href="https:/tienganhmoingay.com/huong-dan/chinh-sach-diem-thuong/?r=footer">Chính
                                                sách tặng điểm thưởng</a></li>
                                        <li>
                                            <a href="https:/tienganhmoingay.com/huong-dan/cac-chinh-sach-uu-dai-khac/?r=footer">Chính
                                                sách ưu đãi khi mua tài khoản</a></li>
                                        <li>
                                            <a href="https:/tienganhmoingay.com/huong-dan/chinh-sach-bao-luu-cong-them-ngay-dung/?r=footer" title="Chiến sách cộng thêm ngày sử dụng">Chính sách bảo lưu &amp; cộng thêm
                                                ngày sử dụng</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item contact-block">
                                <h3 class="link-title">• Chăm sóc khách hàng <span class="icon icon-bt_up hidden-md hidden-lg"></span></h3>
                                <div class="wrap-link">
                                    <ul class="links">
                                        <li><a style="margin: 0; padding: 0;" href="/huong-dan/cach-hoc-tren-tieng-anh-moi-ngay/?r=footer" title="Hướng dẫn cách học">Hướng dẫn cách học</a>
                                        </li>
                                        <li><a style="margin: 0; padding: 0;" href="/pay/?r=footer" title="Hướng dẫn thanh toán">Hướng dẫn thanh toán</a>
                                        </li>
                                    </ul>
                                    <p class="intro-text">Chúng tôi cam kết hỗ trợ bạn <span>tối đa (9am - 9pm)</span></p>
                                    <ul class="links">
                                        <li><a href="javascript:;" title="0916 92 1419"><span class="glyphicon glyphicon-earphone"></span> 0916 92 1419</a>
                                        </li>
                                        <li><a href="/contact/?r=footer" title="Gửi tin"><span class="glyphicon glyphicon-edit"></span>Gửi tin</a>
                                        </li>
                                        <li><a href="javascript:;" title="support@tienganhmoingay.com"><span class="glyphicon glyphicon-envelope"></span>
                                                support@tienganhmoingay.com</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item share-block">
                                <h3 class="link-title">• Kết nối <span class="icon icon-bt_up hidden-md hidden-lg"></span>
                                </h3>
                                <div class="wrap-link">
                                    <ul class="links" style="margin-bottom: 0;">
                                        <li>
                                            <a href="http:/facebook.com/tienganhmoingayonline" target="_blank"><img src="{{url('/uploads/image/facebook_icon.png')}}" alt="Tiếng Anh Mỗi Ngày Facebook Page"></a>
                                        </li>
                                        <li>
                                            <a href="http:/youtube.com/tienganhmoingayonline" target="_blank"><img src="{{url('/uploads/image/youtube_icon.png')}}" alt="Tiếng Anh Mỗi Ngày YouTube "></a>
                                        </li>
                                    </ul>
                                    <p class="intro-text">Like Page để học tiếng Anh hàng ngày</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script src="{{url('/jquery-3.4.1.min.js')}}"></script>
    <script src="https:/cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https:/maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{url('/js/script.js')}}"></script>
    <script>
        let displayProfile = false;
        document.querySelector('.user-login span').addEventListener('click', function() {
            if (displayProfile == true) {
                document.querySelector('.user-info').style.display = 'none';
                displayProfile = false;
            } else {
                document.querySelector('.user-info').style.display = 'block';
                displayProfile = true;
            }
        });
    </script>
    <script type="text/javascript">
        var register = document.querySelector('#user-register');
        var login = document.querySelector('#user-login');
        var formRegister = document.querySelector('#form-register');
        var formLogin = document.querySelector('#form-login');
        formRegister.style.display = "none";
        register.addEventListener('click', function() {
            formLogin.style.display = "none";
            formRegister.style.display = "block";
        })
        login.addEventListener('click', function() {
            formLogin.style.display = "block";
            formRegister.style.display = "none";
        })


        let studentErr = {{json_encode(Session::has('studentErr'))}} || false;

        let modalLogin = document.querySelector("#myModal");

        if (studentErr) {
            
            modalLogin.style.display = "block";

            {{Session::forget('studentErr')}}
        }



    /*======================= REGISTER FORM ======================= */ 
    
        const csrf =  document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        function StudentRegister (){

        let name     = document.querySelector('#form-register input[name="name"]').value;
        let email    = document.querySelector('#form-register input[name="email"]').value;
        let password = document.querySelector('#form-register input[name="password"]').value;
        let phone    = document.querySelector('#form-register input[name="phone"]').value;
        let erros    = document.querySelectorAll(".err-rule");

        if(erros.length > 0)
        {
            console.log(erros);
            
            erros.forEach(err => err.remove());

        }

        fetch("{{route('student-register')}}",
        {
            method: "POST",
            body: JSON.stringify({name, email, password, phone}),
            headers: {
                'content-type': 'application/json',
                'X-CSRF-TOKEN': csrf
            }
        })
        .then(response =>  response.json())
        .then(data => {
            if(data === true){
               location.reload();
            }

            let formRegister = document.querySelector("#form-register p");
            let err = document.createElement("p"); 
            err.classList.add('err-rule');
            err.style.color = "red";

            for(const prop in data){
                err.innerText = data[prop][0];
                break;
            }

            formRegister.after(err);
            
            
        })
        .catch(err => console.log(err));
        
    }   

        
    btnRegister = document.querySelector('#btnRegister');
    btnRegister.addEventListener('click', StudentRegister)        
    
    

    </script>
    @yield('script')
</body>

</html>
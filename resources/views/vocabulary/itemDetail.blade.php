@extends('layouts.app')
@section('title','vocabulary detail')
@section('content')
<main>
    <div class="main-content">
        <div class="container bgr-white">
            <div class="main-head">
                <ul>
                    <li id="learning" class="active-status">Từ: <span
                                style="text-transform: capitalize">{{$post->title}}</span></li>
                </ul>
            </div>
            <div class="topic-content">
                <section class="word-intro">
                    <h1 class="word" style="text-transform: uppercase">
                        {{$post->title}}
                        <span class="pos">
                            ({{$post->word_type}})
                            </span>
                    </h1>
                    <ul class="audio-info-wrapper">
                        <li>
                            <img id="audio" src="uploads/image/audio_icon_blue.png" onclick="playAudio()"
                                 au="{{$post->audio}}"
                                 class="audio-icon main-audio" alt="Nghe phát âm của từ build up">
                            <script>
                                function playAudio() {
                                    var audio = new Audio(document.getElementById("audio").getAttribute("au"));
                                    audio.play();
                                }
                            </script>
                        </li>
                        <li>
                            {{$post->pronounce}}
                        </li>
                        <h4>{{$post->content}}</h4>
                    </ul>
                </section>
                <section class="clearfix word-family-patterns">
                    <div class="usage-patterns">
                        <div class="heading-wrapper">
                            <h1 class="heading text-center">
                                Cách dùng từ vựng
                            </h1>
                        </div>
                        <div class="all-usage-patterns-wrapper">
                            <div class="no-usage-pattern-wrapper">
                                {!! $post->use !!}
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</main>
@endsection
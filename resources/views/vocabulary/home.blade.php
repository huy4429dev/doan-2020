@extends('layouts.app')
@section('title','vocabulary')
@section('content')
<main>
    <div class="main-content">
        <div class="container bgr-white">
            <div class="main-head">
                <ul>
                    <li id="learning" class="active-status" onclick="pickStatus(event)" data-target="content-learn">Đang
                        học: (0)
                    </li>
                </ul>
            </div>
            <div id="content-test" class="content-test">
                <h2>Hôm nay bạn muốn học chủ đề nào?</h2>
            </div>
            <div id="content-learn" class="learning">
                <ul class="text-center">
                    {{--hiển thị các đề tài random--}}
                    @foreach($limit as $item)
                        <li class="head-learning">
                            <a href="tu-vung/{{$item->id}}">
                                <div class="img-radius">
                                    <img src="uploads/vocabulary/{{$item->thumb}}" alt="">
                                </div>
                                <h3>{{$item->title}}</h3>
                            </a>
                        </li>
                    @endforeach
                </ul>
                <div class="border-solid text-center">
                    <h4>Những chủ đề bạn có thể học?</h4>
                </div>
                <div class="bot-learning">
                    {{--hiển thị tất cả các đề tài--}}
                    <ul class="text-center">
                        {{--hiển thị các đề tài random--}}
                        @foreach($topic as $item)
                            <li class="head-learning">
                                <a href="tu-vung/{{$item->id}}">
                                    <div class="img-radius">
                                        <img src="uploads/vocabulary/{{$item->thumb}}" alt="">
                                    </div>
                                    <h3>{{$item->title}}</h3>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div id="content-not-learn" class="not-learn" style="display: none">
                <div class="you-want">
                    <h1>Hôm nay bạn muốn học chủ đề nào?</h1>
                    <div class="topic-want">
                        <div>
                            <a href=""><img src="../asset/uploads/image/conference.png" alt=""></a>
                            <a href="">Conferences</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/electronics.png" alt=""></a>
                            <a href="">Electronics</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/pharmacy.webp" alt=""></a>
                            <a href="">Pharmacy</a>
                        </div>
                    </div>
                    <div class="border-solid">
                        Lựa chọn chủ đề bạn muốn học dưới đây
                    </div>
                    <div class="all-topics">
                        <div>
                            <a href=""><img src="../asset/uploads/image/airlines.png" alt=""></a>
                            <a href="">Airlines</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/airlines.png" alt=""></a>
                            <a href="">Airlines</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/airlines.png" alt=""></a>
                            <a href="">Airlines</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/airlines.png" alt=""></a>
                            <a href="">Airlines</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/airlines.png" alt=""></a>
                            <a href="">Airlines</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/airlines.png" alt=""></a>
                            <a href="">Airlines</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/airlines.png" alt=""></a>
                            <a href="">Airlines</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/airlines.png" alt=""></a>
                            <a href="">Airlines</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/airlines.png" alt=""></a>
                            <a href="">Airlines</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/airlines.png" alt=""></a>
                            <a href="">Airlines</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/airlines.png" alt=""></a>
                            <a href="">Airlines</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/airlines.png" alt=""></a>
                            <a href="">Airlines</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/airlines.png" alt=""></a>
                            <a href="">Airlines</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/airlines.png" alt=""></a>
                            <a href="">Airlines</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/airlines.png" alt=""></a>
                            <a href="">Airlines</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/airlines.png" alt=""></a>
                            <a href="">Airlines</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/airlines.png" alt=""></a>
                            <a href="">Airlines</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/airlines.png" alt=""></a>
                            <a href="">Airlines</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/airlines.png" alt=""></a>
                            <a href="">Airlines</a>
                        </div>
                        <div>
                            <a href=""><img src="../asset/uploads/image/airlines.png" alt=""></a>
                            <a href="">Airlines</a>
                        </div>

                    </div>
                    <h1 id="learn-slogan">Học học nựa , học mại mại !</h1>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
@extends('layouts.app')
@section('title','vocabulary - detail')
@section('content')
<main>
    <div class="main-content">
        <div class="container bgr-white">
            <div class="main-head">
                <ul>
                    <li id="learning" class="active-status" data-target="content-learn">Đang
                        học: ({{$topic->title}})
                    </li>
                </ul>
            </div>
            <div id="content-test" class="content-test">
                <h2>Hôm nay bạn muốn học chủ đề nào?</h2>
            </div>
            <div id="content-learn" class="learning">
                <ul class="text-center mb-0">
                    {{--hiển thị các đề tài random--}}
                    <li class="head-learning">
                        <div class="img-radius">
                            <img src="uploads/vocabulary/{{$topic->thumbnail}}" alt="">
                        </div>
                        <h3>{{$topic->title}}</h3>
                    </li>
                </ul>
                <div class="border-solid text-center">
                    <h4>Những từ vựng liên quan đến chủ đề này</h4>
                </div>
                <div class="bot-learning">
                    {{--hiển thị tất cả các đề tài--}}
                    <ul class="text-center">
                        {{--hiển thị các đề tài random--}}
                        @foreach($post as $item)
                        <li class="head-learning">
                            <a href="tu-vung/item/{{$item->id}}">
                                <h4>{{$item->title}}</h4>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
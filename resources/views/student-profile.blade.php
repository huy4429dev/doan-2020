@extends('layouts.app')
@section('title','Hồ sơ')
@section('content')
    <main>
        <div class="main-content">
            <div class="container bgr-white">
                <div class="article-nav-wrapper">

                    <div class="full-toeic-test box">
                        <div style="margin-bottom: 60px;">
                            <h1 class="text-center text-capitalize pt-5">{{Session::get('user')->name}}</h1>
                            <h4 class="text-center text-capitalize mb-4">New member - English Pro </h4>
                            <div class="about-avatar pt-4 text-center">
                                <img src="../uploads/image/student-avatar.jpg" alt="">
                            </div>
                            <!-- <p class="text-center mx-5 px-5 pt-2 py-5">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi venenatis et tortor ac tincidunt. In euismod iaculis lobortis. Vestibulum posuere molestie ipsum vel sollicitudin. Vestibulum venenatis pharetra mi, ut vestibulum elit ultricies a. Vestibulum
                                at mollis ex, ac consectetur massa. Donec nunc dui, laoreet a nibh et, semper tincidunt nunc. Donec ac posuere tellus. Pellentesque tempus suscipit velit sit amet bibendum.
                            </p> -->
                        </div>
                        <h2>Lịch sử test toeic</h2>
                        @if(!$histories->isEmpty())
                        <table class="table table-sm">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Đề thi</th>
                                    <th scope="col">Kết quả</th>
                                    <th scope="col">Thời gian</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php 
                                    $i = 1;
                                @endphp
                                @foreach($histories as $history)
                                <tr>
                                    <th scope="row">{{$i}}</th>
                                    <td>{{$history->toeicExam->title}}</td>
                                    <td>{{$history->result}}</td>
                                    <td>{{$history->created_at}}</td>
                                </tr>
                                @php 
                                    $i++;
                                @endphp
                                @endforeach
                            </tbody>
                        </table>
                        @else
                            <h4 class="text-warning text-center">Chưa có kết quả</h4>
                        @endif
                    </div>
                </div>
    </main>
@endsection